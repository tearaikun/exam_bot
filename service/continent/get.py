import json


def get_continents():
    with open("db/continents.json", "r") as file:
        data = json.loads(file.read())
    return data.keys()
get_continents()


def get_continent(continent_name) -> dict | None:
    with open("db/continents.json", "r") as file:
        data = json.loads(file.read())
    return data.get(continent_name)