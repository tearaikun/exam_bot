import json

def create(chat_id: str, countries):
    with open(file="db/continents.json", mode="r") as file:
        countries = file.read()
        countries_dict = json.loads(countries)
    countries_dict[chat_id] = {        
        "countries": countries
    }
    with open(file="db/continents.json", mode="w") as file:
        file.write(json.dumps(countries_dict))