import json

def get_continent_countries(continent_name: str) -> dict | None:
    print(continent_name, "get_country")
    with open("db/continents.json", "r") as file:
        cont_dict = json.loads(file.read())
    return cont_dict.get(continent_name)
