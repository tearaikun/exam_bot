import json

def create(countries):
    with open("db/continents.json", "r") as file:
        countr = file.read()
        countries_dict = json.loads(countr)
    countries_dict[countries] = {
        "ru":countries,
    }    

    with open(file="db/continents.json", mode="w") as file:
        file.write(json.dumps(countries_dict))
    