import os

import telebot
from telebot.types import (
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    ReplyKeyboardMarkup
)
from telebot.custom_filters import TextFilter, TextMatchFilter, IsReplyFilter
from telebot import types
from keyboards.inline import inlines
# from service import user
from service import text as tx


TOKEN = "6317128487:AAFboYNVzgDN8zq-vmQ2FmSs30X-iHg9RIg"

bot = telebot.TeleBot(TOKEN)

# users = {}


@bot.message_handler(commands=['start'])
def start_handler(message):
    bot.send_message(chat_id=message.chat.id, text='pick a continent you would like to visit', reply_markup=inlines.get_continent_keyboard())


@bot.callback_query_handler(lambda call: call.data.split("_")[0] == "continent") #call.data.split("_") = ["continent", "some"]
def callback_continent_handler(call: types.CallbackQuery):
    continent_name = call.data.split("_")[1]
    countries_keyboard = inlines.get_continent_countries_keyboard(continent_name)
    bot.edit_message_reply_markup(call.from_user.id, call.message.message_id, reply_markup=countries_keyboard)

@bot.callback_query_handler(lambda call: call.data == "back_continent") 
def callback_query_handler(call: types.CallbackQuery):
    bot.edit_message_reply_markup(call.from_user.id, call.message.message_id, reply_markup=inlines.get_continent_keyboard())



# @bot.message_handler(func=lambda message: message.text in ("send photos"))
# def photo_handler(message: types.Message):
#     images = []
#     for photo in ["media/afghanistan1.jpeg", "media/afghanistan2.jpeg"]:
#             image = open(photo, "rb")   
#             images.append(types.InputMediaPhoto(image))

#     bot.send_media_group(chat_id=message.chat.id, media=images)


@bot.callback_query_handler(func=lambda call: call.data.split("_")[0] == "country")
def photo_callback_handler(call):
    country = call.data.split('_')[1]
    images = os.listdir(f"media/countries/{country}")
    data = []
    for image in images:
        print(image)
        data.append(
            types.InputMediaPhoto(
                open(file=f"media/countries/{country}/{image}", mode="rb")
                )
            )
    bot.send_media_group(chat_id=call.message.chat.id, media=data)

#    добавь условие при котором проект на крашниться, в случае если тактой папки нет

bot.polling(non_stop=True)

