from telebot.types import (
    InlineKeyboardButton,
    InlineKeyboardMarkup
)
from service import continent, country



def get_continent_keyboard():
    keyboard = InlineKeyboardMarkup(row_width=1)
    for i in continent.get_continents():
        keyboard.add(InlineKeyboardButton(text=i, callback_data=f"continent_{i}")) # continent_asia.split("_"), 

    return keyboard

def get_continent_countries_keyboard(continent_name):
    keyboard = InlineKeyboardMarkup(row_width=1)
    for i in country.get_continent_countries(continent_name):
        keyboard.add(InlineKeyboardButton(text=i, callback_data=f"country_{i}"))
    keyboard.add(
        InlineKeyboardButton(text="Назад", callback_data="back_continent")
    )
    return keyboard


# def delete_user():
#     keyboard = InlineKeyboardMarkup(row_width=1)
#     keyboard.add(
#         InlineKeyboardButton(text="Yes", callback_data="delete_yes"),
#         InlineKeyboardButton(text="No", callback_data="delete_no"),
#     )
#     return keyboard